# UI Theme Reporting bug

## Description
The *UITheme* and *UIThemeDisplayed* functions do not behave correctly / consistently based on a user's preferences, or which of LEX and Classic they are viewing. This repository contains the files to reproduce this situation in an sfdx scratch org. The SOQL user preference is always stored correctly though.

From https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_methods_system_userinfo.htm

> **getUiTheme()**
>
> Returns the preferred theme for the current user. Use getUiThemeDisplayed to determine the theme actually displayed to the current user.

> **getUiThemeDisplayed()**
> 
> Returns the theme being displayed for the current user.

## Getting Started
This assumes you know your way around an sfdx setup. If so, it's easy to get it running:

    sfdx force:org:create -f config/project-scratch-def.json -a ui-repro
    sfdx force:source:push -u ui-repro
    sfdx force:org:open -u ui-repro

## Reproduction
* Get your scratch org running
* Open the 'UI Theme Repro (VF)' tab from the app launcher in LEX: `my-scratch-org-id.lightning.force.com/one/one.app#/n/UI_Theme_Repro_VF`
* See that everything looks correct - you are seeing everything set to Theme4d as you would hope, and the setting in SOQL is correct
* Next visit the Visual Force page that backs this (still in LEX mode): `my-scratch-org-id.my.salesforce.com/apex/UIThemeRepro` see that the VisualForce page thinks that the UITheme AND UIThemeDisplayed are both Theme3 (Classic) even though your preference is for LEX. Note that the Lightning Components correctly report Theme4d for your preference
* Next, switch back to Classic and visit the VF page again (`my-scratch-org-id.my.salesforce.com/apex/UIThemeRepro`) as in the previous step. Everything looks right, but the Lightning Component has changed it's mind about what you are seeing
* Now, still in Classic, visit the LEX tab (`my-scratch-org-id.lightning.force.com/one/one.app#/n/UI_Theme_Repro_VF`) from step 2, as it's possible to view LEX pages without having decided to switch fully to LEX. This reports EVERYTHING as being Theme3. Even though you are looking at a LEX themed page 

## Why is this important?
The two different functions (UITheme and UIThemeDisplayed) appear to be returning the same information, whereas the documentation suggests that they should return different information if user preference does not match what's being shown. 

This is particularly problematic when an org is transitioning it's team to LEX, and needs to create intersticial pages that direct a user to either the LEX or VF version of a page depending on whether they are in Classic or not. Currently the only way to do that reliably is to do `[SELECT UserPreferencesLightningExperiencePreferred FROM User WHERE Id = :UserInfo.getUserId()]` which means you have to have an apex controller just for that.


