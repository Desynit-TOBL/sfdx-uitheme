({
    loadPreferences : function(component) {
        var initAction = component.get("c.loadThemePreferences");
        initAction.setParams({ });

        initAction.setCallback(this,function(response){
            if (response.getState() === "SUCCESS"){
                
                component.set("v.UITheme", response.getReturnValue()["UITheme"]);
                component.set("v.UIThemeDisplayed", response.getReturnValue()["UIThemeDisplayed"]);
                component.set("v.userLightningPreference", response.getReturnValue()["userLightningPreference"]);
                
            } else {
                // Yes there should be better error handling!
                console.error("Failed to load preferences this time.");
            }
        });
        $A.enqueueAction(initAction);
    }
})
