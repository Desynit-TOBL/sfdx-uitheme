public with sharing class UIThemeRepro {
    
    @AuraEnabled
    public static Map<String, String> loadThemePreferences() {
        // Load the User Lightning Preference straight from the database
        User currentUser = [
            SELECT UserPreferencesLightningExperiencePreferred FROM User WHERE Id = :UserInfo.getUserId()
        ];
        String userLightningPreference = currentUser.UserPreferencesLightningExperiencePreferred == true ? 'Lightning' : 'Classic';

        return new Map<String, String> {
            'UITheme' => UserInfo.getUiTheme(),
            'UIThemeDisplayed' => UserInfo.getUiThemeDisplayed(),
            'userLightningPreference' => userLightningPreference
        };
    }
}
